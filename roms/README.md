# ROM/EPROM Images

## Owlware HD Basic 3

This is an image of the EPROM found in the floppy disk controller.

File                         | Contents
-----------------------------|-----------
owlware_hdbasic_3_0e2404.bin | Raw binary dump of 2764 EPROM
owlware_hdbasic_3_0e2404.hex | Intel hex format dump of 2764 EPROM. Checksum = 0x0E2404


