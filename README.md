# TRS-80 Color Computer Hard Drive

This is a project to repair and extract data from a third-party hard disk drive system for the TRS-80 Color Computer. The system was purchased on 2014-01-26 through eBay. The package included:

* Hard drive enclosure including:
  * Miniscribe 3425 20M hard drive. Stepper motor positioner. 615 cylinders, 4 heads.
  * Xebec S1410 SASI disk controller.
  * Power supply.
  * Lots of mouse-nibbled wires.
* Hard disk drive interface cartridge in a white painted metal housing.
* Floppy disk controller cartridge with Owl-Ware Hard Drive Basic 3 in ROM. Cartridge labeled Hard Drive Specialist Color Computer Controller.
* A box of floppy disks.
* Sparse paper documentation including very brief details, and some hand-written notes from a previous owner.
* An invoice from Owl Computer Services for purchase of the BASIC ROM, dated 1988-12-21 and shipped 1988-12-27.

## Contents

Directory    | Contents
-------------|-----------
fd_images    | Images of the floppy disks.
hd_images    | Images of the hard drive contents.
manuals      | Various manuals and datasheets accumulated for this project.
roms         | ROM/EPROM images.

## Notes

As received, the Miniscribe 3425 hard drive would blink an error code on its LED at power-up, indicating that it was unable to cover the track zero sensor to its satisfaction. I got the drive to spin up happily by moving the interruptor vane on the stepper motor's outer shaft a bit, and then continuing to fiddle with it until my [MFM Reader/Emulator](http://www.pdp8.net/mfm/mfm.shtml) reported correct cylinder numbers. The drive appears to have MS-DOS for an Epson PC clone on it.
