# Floppy Disk Images

These are images of the floppy disks that came with this system, created with [ImageDisk 1.18](http://www.classiccmp.org/dunfield/img/index.htm).

File       | Picture                                      | Contents
-----------|----------------------------------------------|----------
DISK01.IMD | [![](DISK01_small.jpg?raw=true)](DISK01.jpg) | LR Tech 20 Meg Hard Disk Drivers (with bad sectors)
DISK02.IMD | [![](DISK02_small.jpg?raw=true)](DISK02.jpg) | LRTECH 20 MEG HARD DISK DRIVERS B/U F
DISK03.IMD | [![](DISK03_small.jpg?raw=true)](DISK03.jpg) | OWL WARE UTILITY DISK BACK-UP
DISK04.IMD | [![](DISK04_small.jpg?raw=true)](DISK04.jpg) | Owl Ware's Software Bundle
DISK05.IMD | [![](DISK05_small.jpg?raw=true)](DISK05.jpg) | MULTI-VUE (B) BOOT DISK 6/88 B/U
DISK06.IMD | [![](DISK06_small.jpg?raw=true)](DISK06.jpg) | MULTI-VUE (A) SYSTEM DISK OWL 6/88 B/U
DISK07.IMD | [![](DISK07_small.jpg?raw=true)](DISK07.jpg) | OWL WARE
DISK08.IMD | [![](DISK08_small.jpg?raw=true)](DISK08.jpg) | OWLBASIC FORMATTER
