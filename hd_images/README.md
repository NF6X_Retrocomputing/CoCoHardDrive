# Hard Disk Images Extracted with MFM Reader/Emulator

## Initial Dump, As-Received

The drive that came with this system appears to have been reformatted
for use in an Epson PC clone. It appears to have MS-DOS 2.11 on it,
possibly upgraded to MS-DOS 3.20.

Images created with [ImageDisk 1.18](http://www.classiccmp.org/dunfield/img/index.htm).

File                          | Description
------------------------------|---------------
initial_dump_console.txt      | Console output of mfm_read while imaging the disk
initial_dump_dir_listing.txt  | Directory listing of DOS filesystem (created on a Mac)
initial_dump_emu.gz           | Emulation file (compressed)
initial_dump_extracted.gz     | Extracted data file (compressed)
initial_dump_transitions.gz   | Raw MFM transitions file (compressed)

